serviceGitLab

Jalankan aplikasi dengan menjalankan,
nodemon app.js

Pastikan file .env memiliki semua variabel yang dibutuhkan database

Perhatikan port pada file app.js. Jika terjadi error karena port 3000 sudah digunakan, silakan ganti angka 3000 menjadi anggka di atas 3000. Lalu jalankan kembali aplikasi.

Terima kasih sudah menggunakan service ini.
Silakan hubungi Jeffry jika ada kesulitan menggunakan aplikasi ini di
jeffrywidkaja@gmail.com
