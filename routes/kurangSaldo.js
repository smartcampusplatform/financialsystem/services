const express = require('express')
const router = express.Router()
const db = require('../db')
const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
service: 'gmail',
    auth: {
        user: 'jeffrytugas@gmail.com',
        pass: 'jeffry123'
    }
})
    

router.post('/', function(req, res){
    id = 18216028
    saldo = 1000
    sql = 'UPDATE User SET Saldo = Saldo - ? WHERE User.NIM = ?'
    db.query(sql, [saldo, id], function(err, result) {
        if(err) {
            console.log('Error in querying')
            res.sendStatus(500)
            return false
        }
        db.query('SELECT Saldo, email FROM User WHERE NIM = ?', id, function(err, result){
            if(err) {
                console.log('Error while querying')
                res.sendStatus(500)
                return false
            }
            var mailOptions = {
                from: 'ITBFinance@itb.ac.id',
                to: result[0].email,
                subject: 'Transaksi berhasil',
                text: `Selamat transaksi yang dilakukan berhasil\nSaldo anda sekarang adalah ${result[0].Saldo}`,
            }
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error)
                  res.sendStatus(500)
                  return false
                } else {
                  console.log('Email sent: ' + info.response);
                }
            })
        })
        
        res.sendStatus(200)
    })

})


module.exports = router