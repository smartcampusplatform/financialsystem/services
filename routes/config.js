const express = require('express')
const router = express.Router()
const db = require('../db')

router.get('/', function (req, res) {
    if(req.session.username) {
        NIM = req.session.username
        sql = 'SELECT Pass, email FROM User WHERE NIM = ?'
        db.query(sql, NIM, function(err, result) {
            if(err){
                console.log('Error in querying')
            }
            var profile = {
                NIM     : req.session.username,
                name    : req.session.user.name,
                data    : result
            }
            req.session.email = profile.data[0].email
            res.send(profile)
        })
    }else {
        res.redirect('/login')
    }
})

router.post('/changepass', function(req, res){
    if(req.session.username) {
        var profile = {
            NIM     : req.session.username,
            name    : req.session.user.name,
            email   : req.session.email
        }
        console.log(req.body)
        sql = 'SELECT Pass FROM User WHERE NIM = ?'
        db.query(sql, profile.NIM, function(err, result) {
            if(result.length != 0) {
                if(result[0].Pass == req.body.old){
                    sqlChangePass = parse('UPDATE User Set Pass = ? WHERE User.NIM = %s', profile.NIM) 
                    console.log(sqlChangePass)                
                    db.query(sqlChangePass, req.body.new, function(err, result){
                        if(err){
                            console.log('Error in querying')
                        }
                        console.log(result) 
                        res.send(profile)
                    })
                } else{
                    res.send(profile)
                }
            }
        })
    }else {
        res.redirect('/login')
    }
})

router.get('*', function (req, res) {
    res.sendStatus(404)
})

function parse(str) {
    var args = [].slice.call(arguments, 1),
        i = 0;

    return str.replace(/%s/g, () => args[i++]);
}

module.exports = router