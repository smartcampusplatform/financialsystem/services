const express = require('express')
const router = express.Router()
const db = require('../db')

router.get('/', function(req, res) {
    if(req.session.username) {
        NIM = req.session.username
        console.log(NIM)
        sql = 'SELECT Name, Harga, Tanggal FROM Transaksi WHERE NIM = ?' 
        db.query(sql, NIM, function (err, result) {
            if(err) {
                console.log('Error occured while querying, in Beasiswa page')
            }
            var saldo = {
                name        : req.session.user.name,
                saldo       : req.session.user.saldo,
                transaksi   : result
            }

            res.send(saldo)
        })
        
    }else {
        res.redirect('/login')
    }
})
router.get('*', function (req, res) {
    res.sendStatus(404)
})

module.exports = router