const express = require('express')
const router = express.Router()
const db = require('../db')

router.get('/', function (req, res) {
    if(req.session.username) {
        NIM = req.session.username
        sql =  'SELECT UKT.Id, Name, Semester, Biaya, Status FROM UKT JOIN User ON UKT.NIM = User.NIM WHERE UKT.NIM = ?'
        db.query(sql, NIM, function(err, result) {
            if(err) {
                console.log('Error while querying')
            }
            console.log(result)
            var ukt = {
                NIM         : req.session.username,
                Name        : req.session.user.name,
                ukts        : result
            }
            console.log(result)
            res.send(ukt)
        })
    }else {
        res.redirect('/login')
    }
})

router.get('*', function (req, res) {
    res.sendStatus(404)
})

module.exports = router