const express = require('express')
const router = express.Router()
const db = require('./../db')

router.get('/', makeHomePage)

router.get('*', function (req, res) {
    res.sendStatus(404)
})

function makeHomePage (req, res) {
    console.log('Receive Home Page Request')
    sql = 'SELECT * FROM User WHERE NIM = ?'
    if(req.session.username && req.session.username != 1) {
        db.query(sql, req.session.username, function(err, result) {
            if(err) {
                console.log('Error occured while querying')
            }
            NIM = req.session.username
            //hasil query
            console.log(result)
            name = result[0].Name
            saldo = result[0].Saldo
            sql =  'SELECT UKT.Id, Semester, Status FROM UKT JOIN User ON UKT.NIM = User.NIM WHERE UKT.NIM = ?'
            db.query(sql, NIM, function(err, result) {
                if(err) {
                console.log('Error while querying')
                }
                var user = {
                    name    : name,
                    saldo   : saldo,
                    sem     : result[0].Semester,
                    kode    : result[0].Id,
                    status  : result[0].Status
                    
                }
                req.session.user = user             
                res.send(user)
            }) 
        })
    }else if(req.session.username == 1){
        res.redirect('/admin')
    } else {
        res.redirect('/login')
    }
}

module.exports = router