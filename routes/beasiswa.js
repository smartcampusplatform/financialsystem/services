const express = require('express')
const router = express.Router()
const db = require('./../db')

router.get('/', function (req, res) {
    if(req.session.username) {
        user = req.session.user 
        res.send(user)
    }else {
        res.redirect('/login')
    }
})

router.get('/status', function(req, res) {
    if(req.session.username) {
        NIM = req.session.username
        console.log(NIM)
        sql = 'SELECT Penyedia, Status FROM appliedBeasiswa JOIN Beasiswa ON appliedBeasiswa.Appliance = Beasiswa.IDBeasiswa WHERE NIM = ?' 
        db.query(sql, NIM, function (err, result) {
            if(err) {
                console.log('Error occured while querying, in Beasiswa page')
            }
            console.log('Query succeded, in Beasiswa page')
            console.log('These is the query result :',result)
            appliedBeasiswa = result
            res.send(appliedBeasiswa)
        })
        
    }else {
        res.redirect('/login')
    }
})

router.get('/penawaran', function(req, res) {
    if(req.session.username) {
        res.sendStatus(200)
        
    }else {
        res.redirect('/login')
    }
})

router.get('*', function (req, res) {
    res.sendStatus(404)
})

module.exports = router