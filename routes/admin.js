const express = require('express')
const router = express.Router()
const db = require('../db')
const home = require('./home')
const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
service: 'gmail',
    auth: {
        user: 'jeffrytugas@gmail.com',
        pass: 'jeffry123'
    }
})


router.get('/', function (req, res) {
    console.log('Receive Admin Page Request')
    if(req.session.username == 1) {
        res.sendStatus(200)
    }
    else {
        res.redirect('/home')
    }
})

router.post('/tambahsaldo', function(req, res){
    NIM = req.body.nim
    saldo = req.body.saldo
    console.log(saldo)
    sql = 'UPDATE User SET Saldo = Saldo + ? WHERE User.NIM = ?'
    db.query(sql, [saldo, NIM], function(err, result) {
        if(err) {
            console.log('Error in querying')
            res.sendStatus(500)
            return false
        }
        db.query('SELECT Saldo, email FROM User WHERE NIM = ?', NIM, function(err, result){
            if(err) {
                console.log('Error while querying')
                res.sendStatus(500)
                return false
            }
            var mailOptions = {
                from: 'ITBFinance@itb.ac.id',
                to: result[0].email,
                subject: 'Penambahan Saldo ITB Money',
                text: `Selamat Saldo anda berhasil ditambahkan!\nSaldo anda sekarang adalah ${result[0].Saldo}`,
            }
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error)
                  res.sendStatus(500)
                  return false
                } else {
                  console.log('Email sent: ' + info.response);
                }
            })
            res.send(result)
        })
    })

})


module.exports = router