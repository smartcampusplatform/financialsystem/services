const express = require('express')
const router = express.Router()
const db = require('../db')
const home = require('./home')

router.get('/', function (req, res) {
    console.log('Receive Loginpage Request')
    if(req.session.user){
        res.redirect('/home')
    } else {
        res.sendStatus(200)
    }
})

router.post('/', function(req, res) {
    let username = req.body.loginUsername
    let password = req.body.loginPassword
    console.log(username)
    console.log(password)
    console.log(req.body)
    sql = 'SELECT Pass FROM User WHERE NIM = ?'
    db.query(sql, username, function(err, result) {
        if(err) {
            console.log('Error occured while querying')
        }
        console.log('query succeded')
        if(result.length != 0){
            console.log('Query is not empty')
            console.log('Checking query result')
            if(result[0].Pass == password) {
                console.log('redirect to home page')
                req.session.username =  username
                console.log('this user just login, the id is :',req.session.id)
                res.redirect('/home')            
            }
            else {
                res.sendStatus(403)
            }
        } else {
            res.sendStatus(403)
        }   
    })        
})


router.get('*', function (req, res) {
    res.sendStatus(404)
})

module.exports = router