const db = require('mysql')
//add session
const session = require('express-session')
require('dotenv').config()


//create connection
const connection = db.createConnection({
    host : process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port : process.env.DB_PORT
})


connection.connect(function(err) {
    if(err) {
        console.log('Failed to connect to Database')
        return
    }
    console.log('Connected to Mysql database')
})



module.exports = connection;
