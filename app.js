const express = require('express')
//add session
const session = require('express-session')
//add body parser
const bodyParser = require('body-parser')
//add connection from db
const connection = require('./db')
//add session store
const MySQLStore = require('express-mysql-session')(session)

//Router
//add login handler
const loginRouter = require('./routes/auth')
//add beasiswa handler
const beasiswaRouter = require('./routes/beasiswa')
//add home handler
const home = require('./routes/home')
//add ukt handler
const ukt = require('./routes/ukt')
//add saldo handler
const saldo = require('./routes/saldo')
//add konfigurasi handler
const konfig = require('./routes/config')
//add admin handler
const admin = require('./routes/admin')
//add kurang saldo
const kurangSaldo = require('./routes/kurangSaldo')


//create app as express
const app = express()

//Express use session
app.use(session( {
    secret : process.env.SUPER_SECRET,
    store : new MySQLStore({
        host : process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        port : process.env.DB_PORT},
        connection),
    saveUninitialized : true,
    resave : false
}))

//Express using other library as middleware
//Able to read public folder
app.use(express.static('public'))
//Able to body parse
app.use(bodyParser.urlencoded ({ extended: true }))
//setting to use ejs as view engine
app.set('view engine', 'ejs')

//routing
app.use('/login', loginRouter)
app.use('/beasiswa', beasiswaRouter)
app.use('/home', home)
app.use('/ukt', ukt)
app.use('/saldo', saldo)
app.use('/konfigurasi-akun', konfig)
app.use('/admin', admin)
app.use('/kurangsaldo', kurangSaldo)
app.get('/', function (req, res) {
    res.sendStatus(200)
    console.log('masuk sini gak si')
})

//Special for log out
app.get('/logout', function(req, res) {

    sql = 'DELETE FROM `sessions` WHERE `sessions`.`session_id` = ?'
    connection.query(sql, req.session.id, function(err, result) {
        if(err) {
            console.log('query error')
        }
        console.log('Session in database has ben deleted')
    })
    req.session.destroy()
    res.sendStatus(200)
})

//Listening the request
app.listen(process.env.PORT || 3000, function() {
    console.log('Application is running!')
})
