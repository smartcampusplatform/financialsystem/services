-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: sql12.freemysqlhosting.net
-- Generation Time: May 15, 2019 at 09:03 AM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sql12291247`
--
CREATE DATABASE IF NOT EXISTS `sql12291247` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sql12291247`;

-- --------------------------------------------------------

--
-- Table structure for table `Beasiswa`
--

CREATE TABLE `Beasiswa` (
  `IDBeasiswa` int(11) NOT NULL,
  `Penyedia` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nilai` int(11) NOT NULL,
  `Period` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Beasiswa`
--

INSERT INTO `Beasiswa` (`IDBeasiswa`, `Penyedia`, `Nilai`, `Period`) VALUES
(1, 'Tanoto', 10000000, 1),
(2, 'Permata Bankt, PT', 3000000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Transaksi`
--

CREATE TABLE `Transaksi` (
  `IdTransasksi` int(11) NOT NULL,
  `Harga` int(11) NOT NULL,
  `NIM` int(11) NOT NULL,
  `Tanggal` date NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Transaksi`
--

INSERT INTO `Transaksi` (`IdTransasksi`, `Harga`, `NIM`, `Tanggal`, `Name`) VALUES
(1, 5000, 18216028, '2019-05-01', 'Makan bakwan'),
(2, 5000, 18216028, '2019-05-02', 'makan pisang'),
(3, 10000, 18216032, '2019-05-03', 'Makan Nasi Kantin AWS'),
(4, 3000, 18216032, '2019-05-04', 'Makan Nasi kanti Borju'),
(5, 10000, 18216032, '2019-05-06', 'Makan Nasi bakmoi IWK');

-- --------------------------------------------------------

--
-- Table structure for table `UKT`
--

CREATE TABLE `UKT` (
  `Id` int(11) NOT NULL,
  `Semester` int(11) NOT NULL DEFAULT '1',
  `Biaya` int(11) NOT NULL,
  `NIM` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UKT`
--

INSERT INTO `UKT` (`Id`, `Semester`, `Biaya`, `NIM`, `Status`) VALUES
(1, 7, 5000000, 18216028, 1),
(2, 7, 10000000, 18216032, 0),
(3, 8, 5000000, 18216028, 0);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `NIM` int(11) NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Saldo` int(11) NOT NULL,
  `IDBeasiswa` int(11) DEFAULT NULL,
  `Pass` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`NIM`, `Name`, `Saldo`, `IDBeasiswa`, `Pass`, `email`, `id`) VALUES
(1, 'admin', 9, NULL, 'admin1', 'jeffrytugas@gmail.com', ''),
(18216028, 'Jeffry Khosasi', 60000, 2, 'wakgeng', 'jeffrywidkaja@gmail.com', 'D9 BE 87 AB'),
(18216032, 'Thomas Robin', 1000000, NULL, 'wakgeng1', 'thomasr12@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `appliedBeasiswa`
--

CREATE TABLE `appliedBeasiswa` (
  `Id` int(11) NOT NULL,
  `NIM` int(11) NOT NULL,
  `Appliance` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appliedBeasiswa`
--

INSERT INTO `appliedBeasiswa` (`Id`, `NIM`, `Appliance`, `Status`) VALUES
(1, 18216028, 2, 1),
(2, 18216032, 2, 0),
(3, 18216032, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `expires`, `data`) VALUES
('0Wkj-NUBRthnqwdXAQ3pIAwrkKFWaWY8', 1557933394, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('36TxPgAjwtzodpotE8Y_9fBTr7R2MTBt', 1557944024, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('3OfBGd8Q2bUFl3g3l0wul_19caUCsdMV', 1557923640, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('5iLe-wQb8-V5ggrlmQsou0X9cj4xXdz0', 1557938555, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('5tt_PM6NaoSH9HTyljfrxiiYjNkZh3Iy', 1557967587, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('5xNpw6a9oj_RlCmVgoj2zrI-MUFSPmbd', 1557969441, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"1\"}'),
('6aLXwHD1OiZtLSZSiyRoQAXRl-D4XovD', 1557920750, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('9FpLZg81t-NI10ITcqaA-PNTANoot4Oo', 1557965721, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('CVnw5id1GlSgQt3ZUxAJnGV74vSWCDwX', 1557969291, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('Cn1kKLAMiEfZ1ctgo725kYzkDd0KUbwi', 1557968679, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('Do8dMvkiZ7C5x3Z2NZRJKCI_fb49_vFG', 1557926326, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\",\"user\":{\"name\":\"Jeffry Khosasi\",\"saldo\":56000,\"sem\":7,\"kode\":1,\"status\":1}}'),
('Dqb6gdeyuw9FhH6lwhF0s2i8Bp6O30B8', 1557942625, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('EibmwkBD99i0BFFj_T4oU4eQzkQ2BdYP', 1557966490, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('FlcF59EX9BaePsIhS4N6rGz3BAo6crIh', 1557920873, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('IXJ9dQogiishDDdYyGZMH-GXw-xrvRl4', 1557966842, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('Ox02vsVK-QArMfigTBlzyWSpbsuK39VC', 1557944154, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('QJZPrv-D6D97fPa6dAU8cjtBP9Gc5Fd-', 1557971250, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('QnjfxrJdS7fTpXcauSsRzZ8hBtyKZaY8', 1557925331, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('W_Edx6b3c-FMjdwcDVseYPOsYpQTanE0', 1557938898, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('WngsjBrqw4WaIpVCW2mzTXS5p8rqDKMs', 1557942528, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\"}'),
('XG28JOZ3Fm0WQybuoIA5_JuONk8mnx_L', 1557967363, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('_6amtpu0Irl3OUKD413Wn2-M2W05sSlI', 1557944663, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\"}'),
('bw4tGmy8ySonb-w4Ww6Q8yT1YxdRw1d1', 1557923641, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('dZm5iH2v2Jfc6NhYfh_HZEGEBy9l3bUv', 1557972265, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\",\"user\":{\"name\":\"Jeffry Khosasi\",\"saldo\":60000,\"sem\":7,\"kode\":1,\"status\":1}}'),
('e3wfWs3DS-i8W0krosXfVG-OPQgAwdlZ', 1557969627, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('hGl7lWG5CKygA-CYr7x30a0SDMLvrFRX', 1557973983, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('iGICdp5Ul1LmPeJ2OgdTfg6gtOSDDg-T', 1557969288, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('iPkR6BFkuY3YdC0L-vnFt2DaNvrTeLqT', 1557943829, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('iV6GcoY3u0doZLljSgQCLXC14YAlJb-_', 1557968965, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('msFJeTQtqah2TdKvxafdUkKdYUkWKQVS', 1557965721, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('orsjiYZnRF9y_ELcMUg_0cBCNnfhiw0p', 1557965853, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\",\"user\":{\"name\":\"Jeffry Khosasi\",\"saldo\":56000,\"sem\":7,\"kode\":1,\"status\":1},\"email\":\"jeffrywidkaja@gmail.com\"}'),
('s2HIR6JGpmlY7me-SKqDD1-NR9TT-l_u', 1557965720, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('s_rl0g_rJkbeiYkpO409PwlZprt3prw-', 1557965720, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('toAr_J6zDlrLyp5apq3E7iO5md8S2CHn', 1557939191, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"username\":\"18216028\",\"user\":{\"name\":\"Jeffry Khosasi\",\"saldo\":56000,\"sem\":7,\"kode\":1,\"status\":1}}'),
('tx5hYYa80YmX9BQZpWdBkSKLVd_Kw02Q', 1557942579, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),
('ze0MFFQGsldLZESh5yS0RSU_T85uFJ4t', 1557971173, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Beasiswa`
--
ALTER TABLE `Beasiswa`
  ADD PRIMARY KEY (`IDBeasiswa`);

--
-- Indexes for table `Transaksi`
--
ALTER TABLE `Transaksi`
  ADD PRIMARY KEY (`IdTransasksi`),
  ADD KEY `Transaksi_User_FK` (`NIM`);

--
-- Indexes for table `UKT`
--
ALTER TABLE `UKT`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Dimiliki` (`NIM`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`NIM`),
  ADD KEY `IDBeasiswa` (`IDBeasiswa`);

--
-- Indexes for table `appliedBeasiswa`
--
ALTER TABLE `appliedBeasiswa`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Peminat` (`NIM`),
  ADD KEY `Beasiswa` (`Appliance`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Transaksi`
--
ALTER TABLE `Transaksi`
  MODIFY `IdTransasksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `appliedBeasiswa`
--
ALTER TABLE `appliedBeasiswa`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Transaksi`
--
ALTER TABLE `Transaksi`
  ADD CONSTRAINT `Transaksi_User_FK` FOREIGN KEY (`NIM`) REFERENCES `User` (`NIM`);

--
-- Constraints for table `UKT`
--
ALTER TABLE `UKT`
  ADD CONSTRAINT `Dimiliki` FOREIGN KEY (`NIM`) REFERENCES `User` (`NIM`);

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `User_ibfk_1` FOREIGN KEY (`IDBeasiswa`) REFERENCES `Beasiswa` (`IDBeasiswa`) ON UPDATE CASCADE;

--
-- Constraints for table `appliedBeasiswa`
--
ALTER TABLE `appliedBeasiswa`
  ADD CONSTRAINT `Beasiswa` FOREIGN KEY (`Appliance`) REFERENCES `Beasiswa` (`IDBeasiswa`),
  ADD CONSTRAINT `Peminat` FOREIGN KEY (`NIM`) REFERENCES `User` (`NIM`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
